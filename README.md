ics-ans-role-junos-vlan
===================

Ansible role to create vlans on Junos devices.
- conda create --name junos --file requirements.txt
- conda activate junos

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
#See defaults/main.yml for a working example.
ics_ans_role_junos_user:
ics_ans_role_junos_key:
ics_ans_role_junos_port:
ics_junos_list_vlans:
  - name: vlantest
    vlanid: 1600
  - name: vlanexample
    vlanid: 1601

...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-junos-vlan
```

License
-------

BSD 2-clause
