import os
import testinfra.utils.ansible_runner


host = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_os_shell(host):
    assert os.system("zcat /config/juniper.conf.gz | grep message")


def test_junos_cli(host):
    assert os.system("cli")
